##@ Build Dependencies

## Location to install dependencies to
LOCALBIN ?= $(shell pwd)/bin
$(LOCALBIN):
	mkdir -p $(LOCALBIN)

## Tool Binaries
KUSTOMIZE ?= $(LOCALBIN)/kustomize

## SED usage based on machine architecture
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	SED = gsed
else
	SED = sed
endif

## Tool Versions
# renovate: datasource=github-releases depName=kubernetes-sigs/kustomize extractVersion=^kustomize/(?<version>.+)$
KUSTOMIZE_VERSION ?= v5.6.0

.PHONY: kustomize
kustomize: $(KUSTOMIZE) ## Download kustomize locally if necessary. If wrong version is installed, it will be removed before downloading.
$(KUSTOMIZE): $(LOCALBIN)
	@if test -x $(LOCALBIN)/kustomize && ! $(LOCALBIN)/kustomize version | grep -q $(KUSTOMIZE_VERSION); then \
		echo "$(LOCALBIN)/kustomize version is not expected $(KUSTOMIZE_VERSION). Removing it before installing."; \
		rm -rf $(LOCALBIN)/kustomize; \
	fi
	test -s $(LOCALBIN)/kustomize || GOBIN=$(LOCALBIN) GO111MODULE=on go install sigs.k8s.io/kustomize/kustomize/v5@$(KUSTOMIZE_VERSION)

# renovate: datasource=github-releases depName=helm/helm
HELM_VERSION ?= v3.17.1
HELM ?= $(LOCALBIN)/helm

.PHONY: helm
helm: $(HELM) ## Download helm locally if necessary. If wrong version is installed, it will be removed before downloading.
$(HELM): $(LOCALBIN)
	@if test -x $(LOCALBIN)/helm && ! $(LOCALBIN)/helm version | grep -q $(HELM_VERSION); then \
		echo "$(LOCALBIN)/helm version is not expected $(HELM_VERSION). Removing it before installing."; \
		rm -rf $(LOCALBIN)/helm; \
	fi
	test -s $(LOCALBIN)/helm || \
	  (cd $(LOCALBIN) && curl --location --remote-name "https://get.helm.sh/helm-$(HELM_VERSION)-linux-amd64.tar.gz" && \
	  tar --verbose --extract --gzip --file="helm-$(HELM_VERSION)-linux-amd64.tar.gz" --strip-components=1 linux-amd64/helm && \
	  rm "helm-$(HELM_VERSION)-linux-amd64.tar.gz" && \
	  chmod +x helm)

# Local additions ...
KUBERNETES_SPLIT_YAML ?= $(LOCALBIN)/kubernetes-split-yaml
# renovate: datasource=github-releases depName=yeahdongcn/kubernetes-split-yaml
KUBERNETES_SPLIT_YAML_VERSION ?= v0.4.0
KUSTOHELMIZE ?= $(LOCALBIN)/kustohelmize
# renovate: datasource=github-releases depName=yeahdongcn/kustohelmize
KUSTOHELMIZE_VERSION ?= v0.4.2

define HELM_CHART_DETAILS

home: https://gitlab.com/kubitus-project/external-helm-charts/metal3/#metal3-helm-chart
sources:
- https://gitlab.com/kubitus-project/external-helm-charts/metal3/
maintainers:
- name: Mathieu Parent
  email: math.parent@gmail.com
icon: https://gitlab.com/uploads/-/system/project/avatar/56298367/metal3.png
annotations:
  artifacthub.io/category: integration-delivery
  artifacthub.io/operatorCapabilities: seamless upgrades
endef
export HELM_CHART_DETAILS

.PHONY: kubernetes-split-yaml
kubernetes-split-yaml: $(KUBERNETES_SPLIT_YAML) ## Download kubernetes-split-yaml locally if necessary.
$(KUBERNETES_SPLIT_YAML): $(LOCALBIN)
	test -s $(LOCALBIN)/kubernetes-split-yaml || GOBIN=$(LOCALBIN) go install github.com/yeahdongcn/kubernetes-split-yaml@$(KUBERNETES_SPLIT_YAML_VERSION)

.PHONY: kustohelmize
kustohelmize: $(KUSTOHELMIZE) ## Download kustohelmize locally if necessary.
$(KUSTOHELMIZE): $(LOCALBIN) kubernetes-split-yaml
	test -s $(LOCALBIN)/kustohelmize || GOBIN=$(LOCALBIN) GO111MODULE=on go install github.com/yeahdongcn/kustohelmize@$(KUSTOHELMIZE_VERSION)

# renovate: datasource=github-releases depName=metal3-io/baremetal-operator
BAREMETAL_OPERATOR_VERSION ?= v0.9.0

.PHONY: manifests
manifests:
	rm -rf baremetal-operator && \
	  git clone \
			--quiet \
			--branch "$(BAREMETAL_OPERATOR_VERSION)" \
			--single-branch \
			--depth 1 \
			https://github.com/metal3-io/baremetal-operator.git && \
	    cd baremetal-operator && \
		git remote add sathieu https://github.com/sathieu/baremetal-operator.git && \
		git fetch sathieu && \
		git cherry-pick --no-commit sathieu/disableNameSuffixHash && \
		$(SED) -i 's/#- prometheus/- prometheus/g' config/base/kustomization.yaml && \
		$(SED) -i 's/MARIADB_HOST_IP/127.0.0.1/g' ironic-deployment/components/mariadb/certificate.yaml

.PHONY: generate-baremetal-operator.yaml
generate-baremetal-operator.yaml: kustomize
	cd baremetal-operator/config/overlays && \
	  rm -rf temp && mkdir temp && cd temp && \
	  $(KUSTOMIZE) create --resources=../../overlays/basic-auth_tls

.PHONY: config/baremetal-operator.yaml
config/baremetal-operator.yaml: generate-baremetal-operator.yaml
	$(KUSTOMIZE) build baremetal-operator/config/overlays/temp --output config/baremetal-operator.yaml

.PHONY: baremetal-operator-helm-chart-create
baremetal-operator-helm-chart-create: manifests config/baremetal-operator.yaml kustohelmize ## Create an helm chart.
	rm -rf charts/baremetal-operator && mkdir -p charts/baremetal-operator/templates && \
	  cp src/baremetal-operator/ironic-credentials.yaml charts/baremetal-operator/templates/baremetal-operator-ironic-credentials.yaml && \
	  cp src/baremetal-operator/ironic_config.yaml charts/baremetal-operator/templates/baremetal-operator-ironic_config.yaml && \
	  $(KUSTOHELMIZE) create charts/baremetal-operator \
	  --from=config/baremetal-operator.yaml \
	  --version=0.0.0 \
	  --app-version=$(BAREMETAL_OPERATOR_VERSION) \
	  --description="Metal³ baremetal-operator" && \
	  echo "$$HELM_CHART_DETAILS" >> charts/baremetal-operator/Chart.yaml && \
	  $(SED) -i '78i \          {{- if or .Values.tls.enabled .Values.basicAuth.enabled .Values.extraVolumeMounts .Values.webhook.enabled }}' charts/baremetal-operator/templates/baremetal-operator-controller-manager-deployment.yaml && \
	  $(SED) -i '97i \            {{- end }}' charts/baremetal-operator/templates/baremetal-operator-controller-manager-deployment.yaml && \
	  $(SED) -i '105i \      {{- if or .Values.tls.enabled .Values.basicAuth.enabled .Values.extraVolumes .Values.webhook.enabled }}' charts/baremetal-operator/templates/baremetal-operator-controller-manager-deployment.yaml && \
	  $(SED) -i '125i \        {{- end }}' charts/baremetal-operator/templates/baremetal-operator-controller-manager-deployment.yaml

.PHONY: baremetal-operator-helm-chart-package
baremetal-operator-helm-chart-package: helm
	$(HELM) package charts/baremetal-operator --version=$(HELM_CHART_VERSION) && \
	  $(HELM) template baremetal-operator-*.tgz

.PHONY: generate-ironic.yaml
generate-ironic.yaml: kustomize
	cd baremetal-operator/ironic-deployment/overlays && \
	  rm -rf temp && mkdir temp && cd temp && \
	  cp ../../../../src/ironic/kustomization-patches.yaml ./kustomization-patches.yaml && \
	  $(KUSTOMIZE) create --resources=../../../config/namespace && \
	  $(KUSTOMIZE) edit add resource ../../default && \
	  $(KUSTOMIZE) edit add component ../../components/basic-auth && \
	  $(KUSTOMIZE) edit add component ../../components/keepalived && \
	  $(KUSTOMIZE) edit add component ../../components/mariadb && \
	  $(KUSTOMIZE) edit add component ../../components/tls && \
	  $(KUSTOMIZE) edit add patch --path=kustomization-patches.yaml

.PHONY: config/ironic.yaml
config/ironic.yaml: generate-ironic.yaml
	$(KUSTOMIZE) build baremetal-operator/ironic-deployment/overlays/temp --output config/ironic.yaml

.PHONY: ironic-helm-chart-create
ironic-helm-chart-create: manifests config/ironic.yaml kustohelmize ## Create an helm chart.
	rm -rf charts/ironic && mkdir -p charts/ironic/templates && \
	  cp src/ironic/ironic-htpasswd.yaml charts/ironic/templates/ironic-htpasswd.yaml && \
	  $(KUSTOHELMIZE) create charts/ironic \
	  --from=config/ironic.yaml \
	  --version=0.0.0 \
	  --app-version=$(BAREMETAL_OPERATOR_VERSION) \
	  --description="Metal³ Ironic" && \
	  echo "$$HELM_CHART_DETAILS" >> charts/ironic/Chart.yaml && \
	  $(SED) -i '48i \          {{- if or .Values.tls.enabled .Values.mariadb.enabled }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml && \
	  $(SED) -i '65i \          {{- end }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml && \
	  $(SED) -i -e '132,135d' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml && \
	  $(SED) -i '132i \              {{- if .Values.tls.enabled }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '133i \              command: [ "sh", "-c", "curl -sSfk https://127.0.0.1:6385" ]' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '134i \              {{- else }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '135i \              command: [ "sh", "-c", "curl -sSfk http://127.0.0.1:6180/images" ]' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '136i \              {{- end }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i -e '144,147d' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml && \
	  $(SED) -i '144i \              {{- if .Values.tls.enabled }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '145i \              command: [ "sh", "-c", "curl -sSfk https://127.0.0.1:6385" ]' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '146i \              {{- else }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '147i \              command: [ "sh", "-c", "curl -sSfk http://127.0.0.1:6180/images" ]' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '148i \              {{- end }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '163i \            {{- if .Values.basicAuth.enabled }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '164i \            - name: ironic-htpasswd' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '165i \              mountPath: /auth/ironic' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '166i \              readOnly: true' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '167i \            {{- end }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml  && \
	  $(SED) -i '310i \      {{- if or .Values.keepalived.enabled .Values.dnsmasq.enabled }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml && \
	  $(SED) -i '312i \      {{- end }}' charts/ironic/templates/baremetal-operator-ironic-deployment.yaml

.PHONY: ironic-helm-chart-package
ironic-helm-chart-package: helm
	$(HELM) package charts/ironic --version=$(HELM_CHART_VERSION) && \
	  $(HELM) template ironic-*.tgz

.PHONY: all-helm-charts-create
all-helm-charts-create: baremetal-operator-helm-chart-create ironic-helm-chart-create

.PHONY: all-helm-charts-package
all-helm-charts-package: baremetal-operator-helm-chart-package ironic-helm-chart-package