# Metal3 Helm chart

This is an Helm chart for [Metal3](https://book.metal3.io/).

## Usage

```shell
helm repo add kubitus-metal3 https://gitlab.com/api/v4/projects/56298367/packages/helm/stable

helm install kubitus-metal3/metal3
```

Available values can be seen [here](charts/metal3/values.yaml), or retrieved with:

```shell
helm show values kubitus-metal3/metal3
```
